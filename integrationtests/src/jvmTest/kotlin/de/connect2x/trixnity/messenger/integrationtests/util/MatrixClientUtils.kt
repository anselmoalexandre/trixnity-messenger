package de.connect2x.trixnity.messenger.integrationtests.util

import com.benasher44.uuid.uuid4
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.coroutines.CoroutineScope
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.clientserverapi.client.MatrixClientServerApiClient
import net.folivo.trixnity.clientserverapi.client.UIA
import net.folivo.trixnity.clientserverapi.model.authentication.AccountType
import net.folivo.trixnity.clientserverapi.model.authentication.Register
import net.folivo.trixnity.clientserverapi.model.uia.AuthenticationRequest
import org.jetbrains.exposed.sql.Database
import org.testcontainers.containers.BindMode
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.utility.DockerImageName

const val synapseVersion =
    "v1.88.0" // TODO you should update this from time to time. https://github.com/matrix-org/synapse/releases

fun synapseDocker(useRegistrationToken: Boolean = false): GenericContainer<Nothing> {
    return GenericContainer<Nothing>(DockerImageName.parse("docker.io/matrixdotorg/synapse:$synapseVersion"))
        .apply {
            withEnv(
                mapOf(
                    "VIRTUAL_HOST" to "localhost",
                    "VIRTUAL_PORT" to "8008",
                    "SYNAPSE_SERVER_NAME" to "localhost",
                    "SYNAPSE_REPORT_STATS" to "no",
                    "UID" to "1000",
                    "GID" to "1000"
                ) +
                        if (useRegistrationToken) mapOf("SYNAPSE_CONFIG_PATH" to "data/homeserver_withRegistration.yaml")
                        else emptyMap()
            )
            withClasspathResourceMapping("data", "/data", BindMode.READ_WRITE)
            withExposedPorts(8008)
            waitingFor(Wait.forHealthcheck())
            withNetwork(Network.SHARED)
        }
}

private const val password = "user$1passw0rd"

suspend fun MatrixClientServerApiClient.register(
    username: String? = null,
    password: String,
    deviceId: String? = null
): Result<MatrixClient.LoginInfo> {
    val registerStep = authentication.register(
        password = password,
        username = username,
        deviceId = deviceId,
        accountType = AccountType.USER,
    ).getOrThrow()
    registerStep.shouldBeInstanceOf<UIA.Step<Register.Response>>()
    val registerResult = registerStep.authenticate(AuthenticationRequest.Dummy).getOrThrow()
    registerResult.shouldBeInstanceOf<UIA.Success<Register.Response>>()
    val (userId, createdDeviceId, accessToken) = registerResult.value
    requireNotNull(createdDeviceId)
    requireNotNull(accessToken)
    return Result.success(MatrixClient.LoginInfo(userId, createdDeviceId, accessToken, "displayName", null))
}

fun newDatabase(accountName: String?) =
    Database.connect("jdbc:h2:mem:${accountName?.let { "${it}_${uuid4()}" } ?: uuid4()};DB_CLOSE_DELAY=-1;")

data class StartedClient(
    val scope: CoroutineScope,
    val client: MatrixClient,
    val password: String
)
