package de.connect2x.trixnity.messenger

import kotlin.system.exitProcess

actual fun closeApp() {
    exitProcess(0)
}