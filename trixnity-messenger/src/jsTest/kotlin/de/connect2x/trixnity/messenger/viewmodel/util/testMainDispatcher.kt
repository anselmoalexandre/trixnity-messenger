package de.connect2x.trixnity.messenger.viewmodel.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

actual val testMainDispatcher: CoroutineDispatcher = Dispatchers.Unconfined