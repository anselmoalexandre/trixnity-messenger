package de.connect2x.trixnity.messenger

internal expect suspend fun deleteAccountDataLocally(accountName: String)
