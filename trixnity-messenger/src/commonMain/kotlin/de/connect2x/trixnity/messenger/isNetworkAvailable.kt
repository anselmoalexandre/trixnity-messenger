package de.connect2x.trixnity.messenger

internal expect fun isNetworkAvailable(): Boolean
