package de.connect2x.trixnity.messenger.viewmodel.util

enum class ErrorType {
    JUST_DISMISS, WITH_ACTION
}